<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Informations extends Model
{
    protected $table = "information";
    protected $primaryKey = "idNo";

    public $timestamps = false;
}
