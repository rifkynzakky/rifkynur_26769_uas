<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Informations;

class InformationsController extends Controller
{
    public function index () 
    {
        $Information = Informations::all();
        
        return view(
            "table",
            compact('Information')
        );

    }

    public function posthandling (Request $request) 
    {
        $command = $request["submission"];
        
        if($command == "Update") {
            $id = $request["checkData"];
            $infoUpdated = Informations::find($id);

            return view(
                "form", compact('infoUpdated','command')
            );

        } else if($command == "Delete") {
            $id = $request["checkData"];
            $infoDel = Informations::find($id);

            return view(
                "deleteConfirmation", compact('infoDel','command')
            );

        } else if($command == "Insert"){

            return view("form");
        }
    }

    public function update(Request $request)
    {
        $validatedData = $request->validate([
            'eName' => 'string|required',
            'eJob' => 'string|required',
            'eDept' => 'string|required',
            'eJabat' => 'string|required',
            'eEmail' => 'string|required',
            'eNoHp' => 'numeric|required|min:0',
            'JoinDate' => 'date|required',
        ]);
        
        $id = $request["checkData"];
        $infoUpdated = Informations::find($id);

        $infoUpdated -> nama = $request["eName"];
        $infoUpdated -> job = $request["eJob"];
        $infoUpdated -> department = $request["eDept"];
        $infoUpdated -> jabatan = $request["eJabat"];
        $infoUpdated -> nomorHp = $request["eNoHp"];
        $infoUpdated -> email = $request["eEmail"];
        $infoUpdated -> joinDate = $request["JoinDate"];
        $infoUpdated -> save();

        return $this ->index();
    }

    public function insert(Request $request)
    {
        $validatedData = $request->validate([
            'eName' => 'string|required',
            'eJob' => 'string|required',
            'eDept' => 'string|required',
            'eJabat' => 'string|required',
            'eEmail' => 'string|required',
            'eNoHp' => 'numeric|required|min:0',
            'JoinDate' => 'date|required',
        ]);
        
        $infoInserted = new Informations;
                       
        $infoInserted -> nama = $request["eName"];
        $infoInserted -> job = $request["eJob"];
        $infoInserted -> department = $request["eDept"];
        $infoInserted -> jabatan = $request["eJabat"];
        $infoInserted -> nomorHp = $request["eNoHp"];
        $infoInserted -> email = $request["eEmail"];
        $infoInserted -> joinDate = $request["JoinDate"];
        $infoInserted -> save();

        return $this ->index();
    }

    public function delete(Request $request)
    {
        $command = $request["submission"];

        $id = $request["checkData"];
        $infoDel = Informations::find($id);

        if($command == "Delete") {

            $infoDel -> delete();
            return $this ->index();

        } else if ($command == "Cancel") {

            return $this ->index();

        } else if ($command == "Back") {

            return $this ->index();

        }
    }

}
