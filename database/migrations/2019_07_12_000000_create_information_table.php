<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInformationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Information', function (Blueprint $table) {
            $table->bigIncrements('idNo');
            $table->string('nama');
            $table->string('job');
            $table->string('department');
            $table->string('jabatan');
            $table->bigInteger('nomorHp');
            $table->string('email');
            $table->date('joinDate');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('Information');
    }
}
