<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/',"InformationsController@index");

Route::get('/form', function () {
    return view('form');
});

Auth::routes();

Route::get('/home',"InformationsController@index");
Route::get('/table', "InformationsController@index");

Route::post('/PostEmp', "InformationsController@posthandling");
Route::post('/SubmitUpdated', "InformationsController@update");
Route::get('/PostEmp', function(){
    return view('errorvalidation');
});

Route::post('/PostEmp', "InformationsController@posthandling");
Route::post('/InsertUpdated', "InformationsController@insert");
Route::get('/PostEmp', function(){
    return view('errorvalidation');
});

Route::post('/PostEmp', "InformationsController@posthandling");
Route::post('/deleteEmp', "InformationsController@delete");
