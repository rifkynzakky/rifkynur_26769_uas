@extends("layouts.app")

<html>
	<head>	
		<title>EMPLOYEE DATA</title>
        @include ('defaults.css-load')
        @include ('defaults.js-load')
</head>
<body>
    @include('layouts.app')
    @yield('navbar')

<div class="container">
    @if($errors->any())
        <div>test</div>
    @endif
    <form method="post" id="selectEmployee" action="PostEmp">
    @csrf
        <h1> </h1>
        <input type="submit" name="submission" value="Insert" class="btn btn-warning" >
        <input type="submit" name="submission" value="Update" class="btn btn-primary" >
        <input type="submit" name="submission" value="Delete" class="btn btn-danger" >
        <h1> </h1>

        <table id="tabell" class="table table-striped table-bordered" style="width:100%">
            <thead>
                <tr>
                @auth
                    <th>Check</th>
                @endauth
                    <th>ID</th>
                    <th>Employee Name</th>
                    <th>Job</th>
                    <th>Department</th>
                    <th>Jabatan</th>
                    <th>Nomor HP</th>
                    <th>Email</th>
                    <th>Join Date</th>
                </tr>
            </thead>
            <tbody>
                @foreach($Information as $data)
                <tr>
                    @auth
                    <td class="text-center">
                    <input class="select-checkbox" type="radio" name="checkData" value='{{$data-> idNo}}'>
                    </td>
                    @endauth
                    <td>{{$data -> idNo}}</td>
                    <td>{{$data -> nama}}</td>
                    <td>{{$data -> job}}</td>
                    <td>{{$data -> department}}</td>
                    <td>{{$data -> jabatan}}</td>
                    <td>{{$data -> nomorHp}}</td>
                    <td>{{$data -> email}}</td>
                    <td>{{$data -> joinDate}}</td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </form>
    <script type="text/javascript" charset="utf-8">
       $(document).ready(function() {
				$('#tabell').DataTable();
			} );
    </script>
</div>
</body>
</html>