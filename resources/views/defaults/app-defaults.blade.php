@extends("layouts.app")

<html>
<header>
    @include('layouts.app')
    @yield('navbar')
</header>

<body>
    @yield('contents')
</body>

</html>