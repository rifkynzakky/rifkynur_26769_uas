@extends("layouts.app")

@section("content")
        
        @include ('defaults.css-load')
        @include ('defaults.js-load')

<div class="container">
<div class="grid-x grid-margin-y">
    @if ($errors->any())
    @foreach ($errors->all() as $error)
    <div class="medium-12 cell">
        <span class="badge alert"><i class="fi-x"></i></span>
        {{$error}}
    </div>
    @endforeach
    @else
    <script>
        window.location.href = '{{url("/table")}}'; //using a named route
    </script>
    @endif
    <div class="medium-12 cell">
        <a href="/form" class="btn btn-warning">
            Insert New
        </a>
        <a href="/table" class="btn btn-primary">
            View Data
        </a>
    </div>
</div>
</div>
@endsection