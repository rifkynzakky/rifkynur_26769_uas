@extends("layouts.app")
@extends('defaults.app-defaults')

@section('contents')
<div class="container">

    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

<?php
    $servername = "localhost";
    $username = "root";
    $password = "";
    $dbname = "uas_pemweb";
    $tablename = "information";
    $conn = NULL;   
?>
@if(isset($infoUpdated -> idNo))
    <form method="POST" action="SubmitUpdated">
    @csrf
        <input type=hidden name="checkData" value='{{$infoUpdated -> idNo}}'>
    
            <div class="form-group">
                <label for="eName">Employee Name</label>
                <input type="text" class="form-control" name="eName" id="eName" value='{{$infoUpdated -> nama}}' placeholder="Enter Your Name" >
            </div>

            <div class="form-group">
                <label for="eJob">Job Desk</label>
                <input type="text" class="form-control" name="eJob" id="eJob" value='{{$infoUpdated -> job}}' placeholder="Enter Your Current Job" >
            </div>

            <div class="form-group">
                <label for="eDept">Department</label>
                <input type="text" class="form-control" name="eDept" id="eDept" value='{{$infoUpdated -> department}}' placeholder="Enter Your Department" >
            </div>

            <div class="form-group">
                <label for="eJabat">Jabatan</label>
                <input type="text" class="form-control" name="eJabat" id="eJabat" value='{{$infoUpdated -> jabatan}}' placeholder="Enter Your Status" >
            </div>

            <div class="form-group">
                <label for="eNoHp">Phone Number</label>
                <input type="number" class="form-control" name="eNoHp" id="eNoHp" value='{{$infoUpdated -> nomorHp}}' placeholder="Enter Your Phone Number (Numeric)" >
            </div>

            <div class="form-group">
                <label for="eEmail">Email</label>
                <input type="text" class="form-control" name="eEmail" id="eEmail" value='{{$infoUpdated -> email}}' placeholder="Enter Your Email" >
            </div>

            <div class="form-group">
                <label for="JoinDate">Employee Join Date</label>
                <input type="date" class="form-control" name="JoinDate" id="JoinDate" value='{{$infoUpdated -> joinDate}}' placeholder="Enter The Date You Joined" >
            </div>

            <button type="submit" class="btn btn-primary"> SUBMIT </button>
    </form>

@else

<form method="POST" action="InsertUpdated">
    @csrf
            <div class="form-group">
                <label for="eName">Employee Name</label>
                <input type="text" class="form-control" name="eName" id="eName"  placeholder="Enter Your Name" >
            </div>

            <div class="form-group">
                <label for="eJob">Job Desk</label>
                <input type="text" class="form-control" name="eJob" id="eJob" placeholder="Enter Your Current Job" >
            </div>

            <div class="form-group">
                <label for="eDept">Department</label>
                <input type="text" class="form-control" name="eDept" id="eDept" placeholder="Enter Your Department" >
            </div>

            <div class="form-group">
                <label for="eJabat">Jabatan</label>
                <input type="text" class="form-control" name="eJabat" id="eJabat" placeholder="Enter Your Status" >
            </div>

            <div class="form-group">
                <label for="eNoHp">Phone Number</label>
                <input type="number" class="form-control" name="eNoHp" id="eNoHp" placeholder="Enter Your Phone Number (Numeric)" >
            </div>

            <div class="form-group">
                <label for="eEmail">Email</label>
                <input type="text" class="form-control" name="eEmail" id="eEmail" placeholder="Enter Your Email" >
            </div>

            <div class="form-group">
                <label for="JoinDate">Employee Join Date</label>
                <input type="date" class="form-control" name="JoinDate" id="JoinDate" placeholder="Enter The Date You Joined" >
            </div>

            <button type="submit" class="btn btn-primary"> INSERT </button>
    </form>
@endif
@endsection('contents')