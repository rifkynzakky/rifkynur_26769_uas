@include ('defaults.css-load')
@include ('defaults.js-load')


<div class="container">
@if(isset($infoDel -> idNo))
    <form method="POST" action="deleteEmp">
        @csrf
        <input type=hidden name="checkData" value='{{$infoDel -> idNo}}'>
            <h1>---------------------------------------------------</h1>
            <h1>Are You Sure You Want To Delete This Data?</h1>
            <h1>---------------------------------------------------</h1>
        <input type="submit" name="submission" class="btn btn-danger" value="Delete">
        <input type="submit" name="submission" class="btn btn-primary" value="Cancel">
    </form>

@else

<form method="POST" action="deleteEmp">
@csrf
<h1>Please choose and ID that you want to delete</h1>
<input type="submit" name="submission" class="btn btn-primary" value="Back">
</form>

@endif
</div>